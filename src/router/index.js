import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/formInput1',
    name: 'Form Input1',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput1.vue')
  },
  {
    path: '/formInput2',
    name: 'Form Input2',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput2.vue')
  },
  {
    path: '/formInput3',
    name: 'Form Input3',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput3.vue')
  },
  {
    path: '/formInput4',
    name: 'Form Input4',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput4.vue')
  },
  {
    path: '/formInput5',
    name: 'Form Input5',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput5.vue')
  },
  {
    path: '/formInput6',
    name: 'Form Input6',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput6.vue')
  },
  {
    path: '/formInput7',
    name: 'Form Input7',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput7.vue')
  },
  {
    path: '/formInput8',
    name: 'Form Input8',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput8.vue')
  },
  {
    path: '/formInput9',
    name: 'Form Input9',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput9.vue')
  },
  {
    path: '/formInput10',
    name: 'Form Input10',
    component: () => import(/* webpackChunkName: "about" */ '../views/fInput10.vue')
  },
  {
    path: '/form1',
    name: 'Form 1',
    component: () => import(/* webpackChunkName: "about" */ '../views/form1.vue')
  },
  {
    path: '/form2',
    name: 'Form 2',
    component: () => import(/* webpackChunkName: "about" */ '../views/form2.vue')
  },
  {
    path: '/form3',
    name: 'Form 3',
    component: () => import(/* webpackChunkName: "about" */ '../views/form3.vue')
  },
  {
    path: '/form4',
    name: 'Form 4',
    component: () => import(/* webpackChunkName: "about" */ '../views/form4.vue')
  },
  {
    path: '/datatable1',
    name: 'Data Table',
    component: () => import(/* webpackChunkName: "about" */ '../views/dataTable1.vue')
  },
  {
    path: '/productform1',
    name: 'Product Form 1',
    component: () => import(/* webpackChunkName: "about" */ '../views/productForm1.vue')
  },
  {
    path: '/producttable',
    name: 'Product Table',
    component: () => import(/* webpackChunkName: "about" */ '../views/Product/productTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
