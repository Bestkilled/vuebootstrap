module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160138/learn_bootstrap/'
    : '/'
}
